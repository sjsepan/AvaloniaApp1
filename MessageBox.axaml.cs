using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;

namespace kekekeks//AvaloniaApp1
{
	//Author: @kekekeks (https://stackoverflow.com/users/2231814/kekekeks)
	//Source: https://stackoverflow.com/questions/55706291/how-to-show-a-message-box-in-avaloniaui-beta
	public partial class MessageBox :
        Window
    {
        public enum MessageBoxButtons
        {
            Ok,
            OkCancel,
            YesNo,
            YesNoCancel
        }

        public enum MessageBoxResult
        {
            None,
            Ok,
            Cancel,
            Yes,
            No
        }

        public MessageBox()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public static Task<MessageBoxResult> ShowAsync
        (
            Window parent,
            string text,
            string title,
            MessageBoxButtons buttons
        )
        {
            var msgbox = new MessageBox()
            {
                Title = title
            };
            msgbox.FindControl<TextBlock>("Text").Text = text;
            var buttonPanel = msgbox.FindControl<StackPanel>("Buttons");

            var messageBoxResult = MessageBoxResult.None;//.Ok;

            void AddButton(string caption, MessageBoxResult resultIfSelected, bool isDefaultValue = false)
            {
                var btn = new Button {Content = caption};
                btn.Click += (object sender, RoutedEventArgs e) => {
                    messageBoxResult = resultIfSelected;
                    msgbox.Close();
                };
                buttonPanel.Children.Add(btn);
                if (isDefaultValue)
                    messageBoxResult = resultIfSelected;
            }

            if (buttons == MessageBoxButtons.Ok || buttons == MessageBoxButtons.OkCancel)
                AddButton("Ok", MessageBoxResult.Ok, true);
            if (buttons == MessageBoxButtons.YesNo || buttons == MessageBoxButtons.YesNoCancel)
            {
                AddButton("Yes", MessageBoxResult.Yes);
                AddButton("No", MessageBoxResult.No, true);
            }

            if (buttons == MessageBoxButtons.OkCancel || buttons == MessageBoxButtons.YesNoCancel)
                AddButton("Cancel", MessageBoxResult.Cancel, true);

            var tcs = new TaskCompletionSource<MessageBoxResult>();
            msgbox.Closed += delegate { tcs.TrySetResult(messageBoxResult); };

            if (parent != null)
            {
                msgbox.ShowDialog(parent);
            }
            else
            {
                 msgbox.Show();
            }
            return tcs.Task;
        }

        public async static Task<MessageBoxResult> Show
        (
            Window parent,
            string text,
            string title,
            MessageBoxButtons buttons
        )
        {
            MessageBox msgbox = new()
            {
                Title = title
            };
            msgbox.FindControl<TextBlock>("Text").Text = text;
            StackPanel buttonPanel = msgbox.FindControl<StackPanel>("Buttons");

            MessageBoxResult messageBoxResult = MessageBoxResult.None;//.Ok;

            void AddButton(string caption, MessageBoxResult resultIfSelected, bool isDefaultValue = false)
            {
                Button button = new() { Content = caption};
                button.Click += (object sender, RoutedEventArgs e) =>
                {
                    messageBoxResult = resultIfSelected;
                    msgbox.Close();
                };
                buttonPanel.Children.Add(button);
                if (isDefaultValue)
                {
                    messageBoxResult = resultIfSelected;
                }
            }

            if (buttons == MessageBoxButtons.Ok || buttons == MessageBoxButtons.OkCancel)
            {
                AddButton("Ok", MessageBoxResult.Ok, true);
            }
            if (buttons == MessageBoxButtons.YesNo || buttons == MessageBoxButtons.YesNoCancel)
            {
                AddButton("Yes", MessageBoxResult.Yes);
                AddButton("No", MessageBoxResult.No, true);
            }

            if (buttons == MessageBoxButtons.OkCancel || buttons == MessageBoxButtons.YesNoCancel)
                AddButton("Cancel", MessageBoxResult.Cancel, true);

            if (parent != null)
            {
                await msgbox.ShowDialog(parent);
            }
            else
            {
                 msgbox.Show();
            }
            return messageBoxResult;
        }
    }
}