# Desktop GUI app prototype, on Linux, in C# / DotNet[8] / Avalonia, using VSCode

![AvaloniaApp1.png](./AvaloniaApp1.png?raw=true "Screenshot")

## MessageBox.Avalonia (deprecated; leave as alternative for others to explore)

dotnet add package MessageBox.Avalonia
<https://github.com/AvaloniaCommunity/MessageBox.Avalonia>

## MessageBox by @kekekeks

Author: @kekekeks (<https://stackoverflow.com/users/2231814/kekekeks>)
Source: <https://stackoverflow.com/questions/55706291/how-to-show-a-message-box-in-avaloniaui-beta>

## Avalonia.Edit

dotnet add package Avalonia.AvaloniaEdit --version 11.0.5

## Avalonia Previewer

Their GitHub page indicates that this version (11.0.10) breaks the previewer for now:
"Requires Avalonia v11.0.2 or lower
The extension require Avalonia nuget package v11.0.2 or lower. The latest version v11.0.5 of Avalonia package will break the previewer"
<https://github.com/AvaloniaUI/AvaloniaVSCode>

## Update Avalonia

dotnet add package Avalonia --version 11.0.10
dotnet add package Avalonia.Desktop --version 11.0.10
dotnet add package Avalonia.Diagnostics --version 11.0.10
dotnet add package Avalonia.Themes.Fluent --version 11.0.10
dotnet add package Avalonia.Fonts.Inter --version 11.0.10

## Debugging

"Avalonia has an inbuilt DevTools window which is enabled by calling the attached AttachDevTools() method in a Window constructor. The default templates have this enabled when the program is compiled in DEBUG mode:
To open the DevTools, press F12, or pass a different Gesture to the this.AttachDevTools() method."
<https://docs.avaloniaui.net/docs/getting-started/developer-tools>

## Instructions for downloading/installing .Net 'Core'

<https://dotnet.microsoft.com/download>
<https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website>

## Avalonia reference

<https://docs.avaloniaui.net/>

## Instructions/example of adding Avalonia to dotnet and creating app ui in VSCode

<https://dev.to/carlos487/avalonia-ui-in-ubuntu-getting-started-2fak>

## HelloWorld tutorial for AvaloniaUI/C# on Linux, w/ instructions on adding Avalonia templates to .Net

<https://www.nequalsonelifestyle.com/2019/05/08/avalonia-hello-world/>

## Multiplatform UI Coding with AvaloniaUI in Easy Samples

<https://www.codeproject.com/Articles/5308645/Multiplatform-UI-Coding-with-AvaloniaUI-in-Easy-Sa>

## Issues

- Going from 0.10.12 to 11.0.10 of Avalonia.* packages, got error "Avalonia error AVLN:0004: Unable to find suitable setter or adder for property Closing of type Avalonia.Controls"; solution was to change signature of handler event args from CancelEventArgs to WindowClosingEventArgs.
- run-time issue with same version change is error "Could not load file or assembly 'Avalonia.Visuals, Version=0.10.12.0, Culture=neutral, PublicKeyToken=c8d484a7012f9a8b'. The system cannot find the file specified."; updating Avalonia.Diagnostics resolved, but an empty window is shown. Follow the upgrade directions at the following link. The empty window is solved by change to the FluentTheme tag.
- Apparently there are a number of breaking changes going from v0.10 to v11.0.
See <https://docs.avaloniaui.net/docs/stay-up-to-date/upgrade-from-0.10> and <https://github.com/AvaloniaUI/Avalonia/wiki/Breaking-Changes>
- Replaced calls to the obsolete file/folder dialogs with calls to IStorageProvider dialogs.


## Q&A

<https://github.com/AvaloniaUI/Avalonia/discussions>

## Contact

Steve Sepan
<ssepanus@yahoo.com>
12/11/2024
