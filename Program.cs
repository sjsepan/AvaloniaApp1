﻿using System;
//using System.Diagnostics;
using Avalonia;

namespace AvaloniaApp1
{
    static class Program
    {
        #region Methods
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">string[]</param>
        /// <returns>int</returns>
        [STAThread]
        public static int Main(string[] args)
        {
            int returnValue = -1;//default to fail code
            //Debugger.Launch();//I wanted to see if this will work in VSCode...

            try
            {
                if (args.Length > 1) throw new NotSupportedException();//if no args expected

                //FrameworkInitializationCompleted load MainWindow into desktop.MainWindow
                //composable:each one of these performs a step and returns a reference to the appbuilder
                AppBuilder.Configure<App>() //App is Avalonia.Application
                    .UsePlatformDetect()
                    .WithInterFont()
                    .LogToTrace() //or LogToException()
                    .StartWithClassicDesktopLifetime(args);

                //Debugger.Break();//...it will break here if you run in debug mode :-)

                //return success code
                returnValue = 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
            return returnValue;
        }
        #endregion Methods
    }
}
