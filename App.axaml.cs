using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;

namespace AvaloniaApp1
{
    public partial class App : Application //RCS1043 does not recognize codebehind; ignore
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            // if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime classicDesktopStyleApplicationLifetime)
            {
                //Note:equivalent
                IClassicDesktopStyleApplicationLifetime desktop = classicDesktopStyleApplicationLifetime;

                desktop.MainWindow = new MainWindow();
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}