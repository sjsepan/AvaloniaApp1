#define DEBUG
using System;
using System.Collections.Generic;
using Avalonia;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Media.Imaging;

// using MessageBox.Avalonia;
// using MessageBox.Avalonia.BaseWindows.Base;
// using MessageBox.Avalonia.DTO;
// using MessageBox.Avalonia.Enums;
// using MessageBox.Avalonia.Models;
// using MessageBox.Avalonia.Views;
//using MAE_Icon = MessageBox.Avalonia.Enums.Icon;
using kekekeks;
using Avalonia.Platform.Storage;

namespace AvaloniaApp1
{
	public partial class MainWindow : Window
    {
        private const string ACTION_IN_PROGRESS = " ...";
        private const string ACTION_DONE = " done";
        private const bool StatusBar_MarqueeOn = true;
        private const bool StatusBar_MarqueeOff = false;

        private static bool isHandlingClose;
        private static bool isQuitRequestConfirmed;

        #region declare status
        // TextBlock statusMessage;
        // TextBlock errorMessage;
        // ProgressBar progressBar;
        // Image actionIcon;
        // Image dirtyIcon;
        #endregion declare status

        public MainWindow()
        {
            InitializeComponent();

            InitControls();

            #if DEBUG
            //Avalonia has an inbuilt DevTools window which is enabled by calling the attached AttachDevTools() method in a Window constructor. The default templates have this enabled when the program is compiled in DEBUG mode:
            //To open the DevTools, press F12, or pass a different Gesture to the this.AttachDevTools() method.
            //V11:  InitializeComponent now has a parameter which controls whether DevTools is attached in debug mode whose default is true
            // this.AttachDevTools();
            #endif
        }
        private void InitControls()
        {
            //pre-Find frequently used controls
            //v11: Previously, to find a named control declared in the XAML file, a call to this.FindControl<T>(string name) or this.GetControl<T>(string name) was needed. This is now unnecessary - controls in the XAML file with a Name or x:Name attribute will automatically cause a field to be generated in the class to access the named control (as in WPF/UWP etc).

            #region define status
            //status
            // statusMessage = this.FindControl<TextBlock>("StatusMessage");
            // errorMessage = this.FindControl<TextBlock>("ErrorMessage");
            // progressBar = this.FindControl<ProgressBar>("ProgressBar");
            // actionIcon = this.FindControl<Image>("ActionIcon");
            // dirtyIcon = this.FindControl<Image>("DirtyIcon");
            #endregion define status

            #region define controls
            //controls
            #endregion define controls

            #region bind control events
            // control.Event += ControlName_Event;
            #endregion bind control events
        }

        #region Event Handlers

        #region Form Events
        private void Window_Opened(object sender, EventArgs e)
        {
            try
            {
                Console.WriteLine("Window_Opened");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Window_Opened" + ex.Message);
            }
        }
        public async void Window_Closing(object sender, /*RoutedEventArgs*//*Cancel*/WindowClosingEventArgs e)
        {
            try
            {//TODO:write close-checking around this semaphore
                if (isHandlingClose)
                {
                    if (isQuitRequestConfirmed)
                    {
                        //don't cancel, allow close
                        // isHandlingClose = false;
                    }
                    else
                    {
                        //cancel because user did NOT confirm close request
                        e.Cancel = true;
                    }
                }
                else
                {
                    //cancel because we haven't asked user yet
                    isHandlingClose = true;
                    e.Cancel = true;

                    //trigger FileQuitAction to ask user
                    isQuitRequestConfirmed = await FileQuitAction();
                    if (isQuitRequestConfirmed)
                    {
                        Close();
                    }
                    else
                    {
                        //TODO:reset flags if  quit not confirmed
                        isHandlingClose = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Window_Closing" + ex.Message);
            }
        }
        #endregion Form Events

        #region Menu Events

        public async void MenuFileNew_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("New" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("New");
            await FileNewAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuFileOpen_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Open" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Open");
            await FileOpenAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuFileSave_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Save" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Save");
            await FileSaveAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuFileSaveAs_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Save As" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Save");
            await FileSaveAsAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuFilePrint_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Print" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Print");
            await FilePrintAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuFilePrintPreview_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Print Preview" + ACTION_IN_PROGRESS);
            StartProgressBar();
            //StartActionIcon("Print Preview");
            await FilePrintPreviewAction();
            //StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public /*async*/ void MenuFileQuit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        public async void MenuEditUndo_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Undo" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Undo");
            await EditUndoAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuEditRedo_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Redo" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Redo");
            await EditRedoAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuEditSelectAll_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Select All" + ACTION_IN_PROGRESS);
            StartProgressBar();
            // StartActionIcon("Select All");
            await EditSelectAllAction();
            // StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuEditCut_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Cut" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Cut");
            await EditCutAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuEditCopy_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Copy" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Copy");
            await EditCopyAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuEditPaste_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Paste" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Paste");
            await EditPasteAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuEditPasteSpecial_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Paste Special" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await EditPasteSpecialAction();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuEditDelete_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Delete" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Delete");
            await EditDeleteAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuEditFind_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Find" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Find");
            await EditFindAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuEditReplace_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Replace" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Replace");
            await EditReplaceAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuEditRefresh_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Refresh" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Reload");
            await EditRefreshAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuEditPreferences_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Preferences" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Preferences");
            await EditPreferencesAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuWindowNewWindow_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("New Window" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowNewWindowAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuWindowTile_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Tile" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowTileAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuWindowCascade_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Cascade" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowCascadeAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuWindowArrangeAll_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Arrange All" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowArrangeAllAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuWindowHide_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Hide" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowHideAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuWindowShow_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Show" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowShowAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuHelpContents_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Contents" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Contents");
            await HelpContentsAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuHelpIndex_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Index" + ACTION_IN_PROGRESS);
            StartProgressBar();
            // StartActionIcon("Index");
            await HelpIndexAction();
            // StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuHelpOnlineHelp_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Online Help" + ACTION_IN_PROGRESS);
            StartProgressBar();
            // StartActionIcon("Online Help");
            await HelpOnlineHelpAction();
            // StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuHelpLicenseInformation_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("License Information" + ACTION_IN_PROGRESS);
            StartProgressBar();
            // StartActionIcon("License Information");
            await HelpLicenseInformationAction();
            // StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuHelpCheckForUpdates_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Check For Updates" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await HelpCheckForUpdatesAction();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void MenuHelpAbout_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("About" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("About");
            await HelpAboutAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        #endregion Menu Events

        #region Control Events
        public void CmdRun_Click(object sender, RoutedEventArgs e)
        {
            SetStatusMessage("Run" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("App");
            RunAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        #endregion Control Events

        #endregion Event Handlers

        #region Methods

        #region Actions
        private static async Task DoSomething()
        {
            for (int i = 0; i < 3; i++)
            {
                //TODO:DoEvents...
                await Task.Delay(1000);
            }
        }

        private void RunAction()
        {
            chkStillAnotherBoolean.IsChecked = chkSomeOtherBoolean.IsChecked;
            chkSomeOtherBoolean.IsChecked = chkSomeBoolean.IsChecked;
            chkSomeBoolean.IsChecked = !chkSomeBoolean.IsChecked;
        }
        private async Task FileNewAction()
        {
            await DoSomething();
        }

        // [Obsolete]
        private async Task FileOpenAction()
        {
			try
			{
                //these simple calls are now 'obsolete', as of v11.0.x
                // OpenFileDialog openDialog = new()
                // {
                // 	Title = "Open...",//"Open a file"
                // 					  //AllowedFileTypes = new string[] {},
                // 					  // AllowsOtherFileTypes = true,
                // 					  //Filters = ...
                // 	Directory = Environment.GetFolderPath(Environment.SpecialFolder.Personal),//".",
                // 	InitialFileName = "",
                // 	AllowMultiple = false
                // };
                // openDialog.Filters.Add(new FileDialogFilter(){Name="*.*", Extensions=?});
                // string[] fileResults = await openDialog.ShowAsync(this);

                //IStorageProvider is the new direction indicated by AvaloniaUI,
                TopLevel topLevel = /*TopLevel.*/GetTopLevel(this);
                FilePickerOpenOptions options = new()
                {
                    Title =  "Open file...",//"Open a file"
                    //Environment.GetFolderPath(Environment.SpecialFolder.Personal)
                    // FileTypeFilter.?,
                    // SuggestedStartLocation = Avalonia.Platform.Storage.IStorageFolder.?  WellKnownFolder.Documents,//".",
                    SuggestedFileName = "",
					AllowMultiple = false
                };
                IReadOnlyList<IStorageFile> fileResults = await topLevel.StorageProvider.OpenFilePickerAsync(options);

				if (fileResults != null)
                {
                    if (fileResults.Count>0)
                    {
                        if (!string.IsNullOrWhiteSpace(fileResults[0].Path.AbsolutePath))
                        {
                            SetStatusMessage(GetStatusMessage() + "'" + fileResults[0].Path.AbsolutePath + "'... ");
                        }
                        else
                        {
                            SetStatusMessage(GetStatusMessage() + " cancelled... ");
                        }
                    }
                    else
                    {
                        SetStatusMessage(GetStatusMessage() + " cancelled... ");
                    }
                }
                else
                {
                    SetStatusMessage(GetStatusMessage() + " cancelled... ");
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Exception: {0}",ex.Message);
            }
        }

        // [Obsolete]
		private async Task FileSaveAction(bool isSaveAs=false)
        {
			try
			{
                //these simple calls are now 'obsolete', as of v11.0.x
				// SaveFileDialog saveDialog = new()
				// {
				// 	Title = isSaveAs ? "Save As..." : "Save file...",
				// 	//AllowedFileTypes = new string[] {},
				// 	// AllowsOtherFileTypes = true,
				// 	//Filters = ...
				// 	Directory = "~",
				// 	InitialFileName = ""
				// };
				// saveDialog.Filters.Add(new FileDialogFilter(){Name="*.*", Extensions=?});
				// string fileResult = await saveDialog.ShowAsync(this);

                //IStorageProvider is the new direction indicated by AvaloniaUI,
                TopLevel topLevel = /*TopLevel.*/GetTopLevel(this);
                FilePickerSaveOptions options = new()
                {
                    Title = "Save file..."
                };
                IStorageFile fileResult = await topLevel.StorageProvider.SaveFilePickerAsync(options);
				if (fileResult != null)
                {
                    if (!string.IsNullOrWhiteSpace(fileResult.Path.AbsolutePath))
                    {
                        SetStatusMessage(GetStatusMessage() + "'" + fileResult.Path.AbsolutePath + "'... ");
                    }
                    else
                    {
                        SetStatusMessage(GetStatusMessage() + " cancelled... ");
                    }
                }
                else
                {
                    SetStatusMessage(GetStatusMessage() + " cancelled... ");
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Exception: {0}",ex.Message);
            }
        }

		private async Task  FileSaveAsAction()
        {
            await FileSaveAction(true);
        }
        private static async Task  FilePrintAction()
        {
            await DoSomething();
            //FilePrintAction(false);
        }
        private static async Task  FilePrintPreviewAction()
        {
            await DoSomething();
            //FilePrintAction(true);
        }

        /// <summary>
        /// Returns true = Yes = Quit
        /// Note:https://github.com/AvaloniaUI/Avalonia/issues/4070
        /// </summary>
        private async Task<bool> FileQuitAction()
        {
            bool returnValue = false;
			try
			{
                SetStatusMessage("Quit" + ACTION_IN_PROGRESS);
                StartProgressBar();

				MessageBox.MessageBoxResult messageBoxResult = await MessageBox.Show(this, "Quit?", Title, MessageBox.MessageBoxButtons.YesNo);
				if (messageBoxResult != MessageBox.MessageBoxResult.None)
                {
                    if (messageBoxResult == MessageBox.MessageBoxResult.Yes)
                    {
                        returnValue = true;
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ACTION_DONE);
                    }
                    else
                    {
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + "cancelled" + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                }
                else
                {
                    throw new Exception("FileQuitAction:messageBoxResult:"+messageBoxResult.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Exception: {0}",ex.Message);
            }
            return returnValue;
        }
        private async Task EditUndoAction()
        {
            await DoSomething();
        }
        private async Task EditRedoAction()
        {
            await DoSomething();
        }
        private async Task EditSelectAllAction()
        {
            await DoSomething();
        }
        private async Task EditCutAction()
        {
            await DoSomething();
        }
        private async Task EditCopyAction()
        {
            await DoSomething();
        }
        private async Task EditPasteAction()
        {
            await DoSomething();
        }
        private async Task EditPasteSpecialAction()
        {
            await DoSomething();
        }
        private async Task EditDeleteAction()
        {
            await DoSomething();
        }
        private async Task EditFindAction()
        {
            await DoSomething();
        }
        private async Task EditReplaceAction()
        {
            await DoSomething();
        }
        private async Task EditRefreshAction()
        {
            await DoSomething();
        }

        // [Obsolete]
		private async Task EditPreferencesAction()
        {
			//do a folder-path dialog demo for preferences
			try
			{
                //these simple calls are now 'obsolete', as of v11.0.x
				// OpenFolderDialog folderDialog = new()
				// {
				// 	Title = "Open Folder...",
				// 	//AllowedFileTypes = new string[] {},
				// 	// AllowsOtherFileTypes = true,
				// 	//Filters = ...
				// 	Directory = "~"
				// };
				// openDialog.Filters.Add(new FileDialogFilter(){Name="*.*", Extensions=?});
				// string folderResult = await folderDialog.ShowAsync(this);

                //IStorageProvider is the new direction indicated by AvaloniaUI,
                TopLevel topLevel = /*TopLevel.*/GetTopLevel(this);
                FolderPickerOpenOptions options = new()
                {
                    Title = "Open folder..."
                };
                IReadOnlyList<IStorageFolder> folderResults = await topLevel.StorageProvider.OpenFolderPickerAsync(options);

				if (folderResults != null)
                {
                    if (folderResults.Count>0)
                    {
                        if (!string.IsNullOrWhiteSpace(folderResults[0].Path.AbsolutePath))
                        {
                            SetStatusMessage(GetStatusMessage() + folderResults[0].Path.AbsolutePath);
                        }
                        else
                        {
                            SetStatusMessage(GetStatusMessage() + " cancelled ");
                        }
                    }
                    else
                    {
                        SetStatusMessage(GetStatusMessage() + " cancelled... ");
                    }
                }
                else
                {
                    SetStatusMessage(GetStatusMessage() + " cancelled... ");
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private async Task WindowNewWindowAction()
        {
            await DoSomething();
        }
        private async Task WindowTileAction()
        {
            await DoSomething();
        }
        private async Task WindowCascadeAction()
        {
            await DoSomething();
        }
        private async Task WindowArrangeAllAction()
        {
            await DoSomething();
        }
        private async Task WindowHideAction()
        {
            await DoSomething();
        }
        private async Task WindowShowAction()
        {
            await DoSomething();
        }
        private async Task HelpContentsAction()
        {
            await DoSomething();
        }
        private async Task HelpIndexAction()
        {
            await DoSomething();
        }
        private async Task HelpOnlineHelpAction()
        {
            await DoSomething();
        }
        private async Task HelpLicenseInformationAction()
        {
            await DoSomething();
        }
        private async Task HelpCheckForUpdatesAction()
        {
			try
			{
				MessageBox.MessageBoxResult messageBoxResult = await MessageBox.Show(this, "Check for Updates?", Title, MessageBox.MessageBoxButtons.YesNo);
                if (messageBoxResult != MessageBox.MessageBoxResult.None)
                {
                    SetStatusMessage(GetStatusMessage() + messageBoxResult.ToString() + ACTION_IN_PROGRESS);
                }
                else
                {
                    throw new Exception("HelpCheckForUpdatesAction:messageBoxResult:"+messageBoxResult.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Exception: {0}",ex.Message);
            }
        }
        private async Task HelpAboutAction()
        {
			try
			{
				MessageBox.MessageBoxResult messageBoxResult = MessageBox.MessageBoxResult.None;
				const string message = "Avalonia Gui App1\nVersion 0.2\nby Stephen J Sepan\nUsing AvaloniaUI";
				messageBoxResult = await MessageBox.Show(this, message, Title, MessageBox.MessageBoxButtons.Ok);
                if (messageBoxResult != MessageBox.MessageBoxResult.None)
                {
                    SetStatusMessage(GetStatusMessage() + messageBoxResult.ToString() + ACTION_IN_PROGRESS);
                }
                else
                {
                    throw new Exception("HelpAboutAction:messageBoxResult:"+messageBoxResult.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Exception: {0}",ex.Message);
            }
        }
        #endregion Actions

        #region Utility

        private string GetStatusMessage()
        {
            return StatusMessage.Text;
        }

        private void SetStatusMessage(string statusMessage)
        {
            StatusMessage.Text = statusMessage;

            InvalidateVisual();
        }

        private void SetErrorMessage(string errorMessage)
        {
            ErrorMessage.Text = errorMessage;

            InvalidateVisual();
        }

        private void StartProgressBar()
        {
            // _progressBar.Fraction = 0.33;
            // _progressBar.PulseStep=0.1;
            // _progressBar.Visible = true;
            // _progressBar.Pulse();
            ProgressBar.IsVisible = StatusBar_MarqueeOn;

            InvalidateVisual();
        }

        private void StopProgressBar()
        {
            // _progressBar.Pulse();
            //Application.DoEvents();
            // _progressBar.Visible = false;
            // _progressBar.Fraction = 0.0;
            // _progressBar.PulseStep=0.0;
            ProgressBar.IsVisible = StatusBar_MarqueeOff;

            InvalidateVisual();
        }

        private void StartActionIcon(string actionName)
        {//name:/Resources/App.png, path: /home/ssepan/Projects/DotNetCoreProjects/AvaloniaApp1/Resources/App.png
            ActionIcon.Source = new Bitmap(string.Format("Resources/{0}.png", string.IsNullOrWhiteSpace(actionName) ? "App" : actionName));
            ActionIcon.IsVisible = true;

            InvalidateVisual();
        }

        private void StopActionIcon()
        {
            ActionIcon.Source = null;
            ActionIcon.IsVisible = false;

            InvalidateVisual();
        }

        private void SetDirtyIcon(bool isSet)
        {
            DirtyIcon.IsVisible = isSet;

            InvalidateVisual();
        }

        #endregion Utility
        #endregion Methods

    }
}